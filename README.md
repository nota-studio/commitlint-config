# commitlint-config

Not a Studio's Commitlint shared config.

## Usage

Install the package by running:

```sh
yarn add -D @notastudio/commitlint-config
```

Then add the extends to your `commitlint.config.js`:

```js
module.exports = {
  extends: ['@notastudio'],
  rules: {
    // your overrides
  }
};
```

Finally, add the following git hook at your `package.json`:

```json
{
  "husky": {
    "hooks": {
      "commit-msg": "commitlint -E HUSKY_GIT_PARAMS"
    }
  }
}
```

You can test the hook by simply committing.

## License

MIT
